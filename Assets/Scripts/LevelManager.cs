﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour
{
    bool gameover = false;
    public int score = 0;
    public void endgame()
    {
        if (!gameover)
        {
            Debug.Log("Restart");
            gameover = true;
            Invoke("restart", 2f);
        }

    }
    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
    }
}
