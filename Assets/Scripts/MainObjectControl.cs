﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainObjectControl : MonoBehaviour
{
    [SerializeField]
    public Transform[] waypoints;
    public bool move = false;
    [SerializeField]
    private float moveSpeed = 5f;
    public GameObject levelmanager;
    public GameObject PinkObject;
    public GameObject GreenObject;
    public GameObject Diamond;
    public GameObject path;
    public TMPro.TextMeshProUGUI Score;



    private int waypointIndex = 0;
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "obj")
        {
            Debug.Log("carpisma");
            levelmanager.GetComponent<LevelManager>().endgame();
            move = false;
            PinkObject.GetComponent<FollowPath>().move = false;
            GreenObject.GetComponent<FollowPath>().move = false;
        }

        if (coll.gameObject.tag == "egg")
        {
            Debug.Log("egg");
            PinkObject.GetComponent<FollowPath>().move = false;
            GreenObject.GetComponent<FollowPath>().move = false;
            Destroy(coll.gameObject);
            Diamond.SetActive(true);
            gameObject.SetActive(false);
            levelmanager.GetComponent<LevelManager>().score += 5000 / path.GetComponent<CreatorPath>().index;
            Score.text = "SCORE     " + levelmanager.GetComponent<LevelManager>().score;
        }
    }

    private void Start()
    {
        Score.text = "SCORE     " + levelmanager.GetComponent<LevelManager>().score;
        transform.position = waypoints[waypointIndex].transform.position;
    }

    private void Update()
    {
        if (move)
            Move();
    }
    public void Go_Button()
    {
        move = true;
    }
    private void Move()
    {
        if (waypointIndex <= waypoints.Length - 1)
        {
            Vector3 relativeVector = transform.InverseTransformPoint(waypoints[waypointIndex].position);
            float newSteer = relativeVector.x;

            transform.Rotate(0, 0, newSteer * 10);
            transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].transform.position, moveSpeed * Time.deltaTime);

            if (transform.position == waypoints[waypointIndex].transform.position)
            {
                waypointIndex += 1;
            }
        }
    }
}
