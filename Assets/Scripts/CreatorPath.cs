﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatorPath : MonoBehaviour
{
    public Material[] materials;

    public GameObject Rocket;
    public GameObject MainLR_Test;
    public int index = 1;


    void Start()
    {
        GameObject newObj = new GameObject("waypoint");
        newObj.transform.position = Rocket.transform.position;
        newObj.transform.parent = transform;

        MainLR_Test.GetComponent<Lr_Test>().points = new Transform[1];
        MainLR_Test.GetComponent<Lr_Test>().points[0] = newObj.transform;

        Rocket.GetComponent<MainObjectControl>().waypoints = new Transform[1];
        Rocket.GetComponent<MainObjectControl>().waypoints[0] = newObj.transform;
    }
    public void undo()
    {
        if (index > 1)
        {
            index--;
            MainLR_Test.GetComponent<Lr_Test>().points = new Transform[index];
            Rocket.GetComponent<MainObjectControl>().waypoints = new Transform[index];

            for (int x = 0; x < index; x++)
            {
                MainLR_Test.GetComponent<Lr_Test>().points[x] = transform.GetChild(x);
                Rocket.GetComponent<MainObjectControl>().waypoints[x] = transform.GetChild(x);
            }
            Destroy(transform.GetChild(index));
            transform.GetChild(index).parent = null;
        }
    }

    void Update()
    {
        ///// Mobile Input /////
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider.transform.tag == "Grid")
            {
                if ((Vector2.Distance(MainLR_Test.GetComponent<Lr_Test>().points[index - 1].position, hit.collider.transform.position) < 2.5f) && (Vector2.Distance(MainLR_Test.GetComponent<Lr_Test>().points[index - 1].position, hit.collider.transform.position) > 1.0f))
                {
                    hit.collider.GetComponent<Renderer>().sharedMaterial = materials[1];

                    MainLR_Test.GetComponent<Lr_Test>().points = new Transform[index + 1];
                    Rocket.GetComponent<MainObjectControl>().waypoints = new Transform[index + 1];

                    GameObject newObj = new GameObject("waypoint" + (index + 1));
                    newObj.transform.position = hit.collider.transform.position;
                    newObj.transform.parent = transform;

                    for (int x = 0; x <= index; x++)
                    {
                        MainLR_Test.GetComponent<Lr_Test>().points[x] = transform.GetChild(x);
                        Rocket.GetComponent<MainObjectControl>().waypoints[x] = transform.GetChild(x);
                    }
                    index++;
                }
            }
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider.transform.tag == "Grid")
            {
                hit.collider.GetComponent<Renderer>().sharedMaterial = materials[0];
            }
        }

        /////// PC Input ///////
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider.transform.tag == "Grid")
            {
                if ((Vector2.Distance(MainLR_Test.GetComponent<Lr_Test>().points[index - 1].position, hit.collider.transform.position) < 2.5f) && (Vector2.Distance(MainLR_Test.GetComponent<Lr_Test>().points[index - 1].position, hit.collider.transform.position) > 1.0f))
                {
                    hit.collider.GetComponent<Renderer>().sharedMaterial = materials[1];

                    MainLR_Test.GetComponent<Lr_Test>().points = new Transform[index + 1];
                    Rocket.GetComponent<MainObjectControl>().waypoints = new Transform[index + 1];

                    GameObject newObj = new GameObject("waypoint" + (index + 1));
                    newObj.transform.position = hit.collider.transform.position;
                    newObj.transform.parent = transform;

                    for (int x = 0; x <= index; x++)
                    {
                        MainLR_Test.GetComponent<Lr_Test>().points[x] = transform.GetChild(x);
                        Rocket.GetComponent<MainObjectControl>().waypoints[x] = transform.GetChild(x);
                    }
                    index++;
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider.transform.tag == "Grid")
            {
                hit.collider.GetComponent<Renderer>().sharedMaterial = materials[0];
            }
        }

    }
}
