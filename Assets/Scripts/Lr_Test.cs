﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lr_Test : MonoBehaviour
{
    [SerializeField] public Transform[] points;
    [SerializeField] private LineController line;

    private void Update()
    {
        line.SetUpLine(points); 
    }
}
