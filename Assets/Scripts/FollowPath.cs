﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    [SerializeField]
    public Transform[] waypoints;
    public bool move = false;
    [SerializeField]
    private float moveSpeed = 5f;

    private int waypointIndex = 0;
    private void Start()
    {
        transform.position = waypoints[waypointIndex].transform.position;
    }
    private void Update()
    {
        if (move)
            Move();
    }
    public void Go_Button()
    {
        move = true;
    }
    private void Move()
    {
        if (waypointIndex <= waypoints.Length - 1)
        {
            Vector3 relativeVector = transform.InverseTransformPoint(waypoints[waypointIndex].position);
            float newSteer = relativeVector.x;
            transform.Rotate(0, 0, newSteer * 10);
            transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].transform.position, moveSpeed * Time.deltaTime);
            if (transform.position == waypoints[waypointIndex].transform.position)
            {
                waypointIndex += 1;
            }
        }
    }
}
